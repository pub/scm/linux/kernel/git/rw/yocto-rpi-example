DESCRIPTION = "itsyndikat image"
IMAGE_FSTYPES = "squashfs wic.bz2"

require recipes-core/images/core-image-minimal.bb

LICENSE = "MIT"

RPI_STUFF = " \
	linux-firmware-rpidistro-bcm43430 \
	linux-firmware-rpidistro-bcm43455 \
	linux-firmware-rpidistro-bcm43456 \
	python3-image \
	python3-pip \
	python3-spidev \
	raspi-gpio \
	rpi-gpio \
	udev-rules-rpi \
	"

IMAGE_INSTALL += " \
	bash \
	bash-completion \
	coreutils \
	glibc-utils \
	kernel-modules \
	less \
	localedef \
	ncurses \
	ncurses-terminfo \
	ncurses-tools \
	openssh \
	procps \
	udev-extraconf \
	util-linux \
	wireless-regdb-static \
	wpa-supplicant \
	"

IMAGE_INSTALL += "${RPI_STUFF}"

IMAGE_FEATURES += " \
	allow-empty-password \
	empty-root-password \
	package-management \
	splash \
"
