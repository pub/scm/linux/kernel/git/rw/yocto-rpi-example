#!/bin/sh
mkdir -p /proc
mkdir -p /ro
mkdir -p /rw
mkdir -p /new_root

mount -t proc none /proc
mount -t devtmpfs none /dev

sqfs="/dev/mmcblk0p2"
rwfs="/dev/mmcblk0p3"

while [ ! -e ${rwfs} ]; do
        echo "Waiting for ${rwfs}"
        sleep 1
done

mount -t ext4 ${rwfs} /rw

# Insert update concept here
if [ -r /rw/root.sfs ] ; then
	mount -t squashfs -o loop /rw/root.sfs /ro
else
	mount -t squashfs ${sqfs} /ro
fi

mkdir -p /rw/overlay
mkdir -p /rw/overlay_work

mount -t overlay overlay -o lowerdir=/ro,upperdir=/rw/overlay,workdir=/rw/overlay_work /new_root/

exec switch_root /new_root /sbin/init
