SUMMARY = "init for its initramfs"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"
SRC_URI = "file://init.sh"
S = "${WORKDIR}"

FILES_${PN} = "/init /dev"

do_install () {
        install -m 0755 ${WORKDIR}/init.sh ${D}/init
        install -d ${D}/dev
        mknod -m 622 ${D}/dev/console c 5 1
}
