BASE="$(pwd)"

export TEMPLATECONF="$(pwd)/meta-itsyndikat/conf/"

mkdir -p ${BASE}/downloads
mkdir -p ${BASE}/sstate

. poky/oe-init-build-env builddir

cat << EOF > conf/auto.conf
MACHINE = "raspberrypi4-64"
DISTRO = "its"
hostname_pn-base-files = "trollhost"
KERNEL_MODULE_AUTOLOAD += " brcmfmac"
DL_DIR = "${BASE}/downloads"
SSTATE_DIR = "${BASE}/sstate"
CMDLINE_SERIAL = "console=serial0,115200"
RPI_EXTRA_CONFIG = "dtoverlay=disable-bt\n"
WKS_FILE = "sdimage-its.wks"
IMAGE_BOOT_FILES = "\${BOOTFILES_DIR_NAME}/* \
                 \${@make_dtb_boot_files(d)} \
                 Image-initramfs-\${MACHINE}.bin;kernel8.img"
EOF
